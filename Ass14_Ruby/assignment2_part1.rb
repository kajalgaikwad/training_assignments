array_1 = [2, 4, 6, 8, 10]
array_2 = [1, 5, 6, 8, 11, 12]

hash_1 = {a: 'a', b: 'b', c: 'c', d: 'd', e: 'e'}
hash_2 = {x: '10', y: '20', z: '30'}

10.times{puts "Hello World"}

print "Range of 30 to 40 numbers\n"
puts (30..40).to_a

print "unique no from two array\n"
array_3 = array_1+array_2
puts array_3.uniq

print "Even numbers\n"
puts array_3.select{|ele| ele.even?}

print " Delete greater than 8 numbers\n"
puts array_3.reject{|ele| ele>8}

print "Sum of cube of element\n"
puts array_1.reduce(0){|sum,x| sum+ x*x*x}

print "Index of '8' element\n"
puts array_3.index(8)

print "Add 5 in all elements of array_1\n"
puts array_1.map{|ele| ele+5}

print "replace array_1 values with hash_1 values"
hash_3=hash_1.keys
puts h=Hash[hash_3.zip array_1]
