//Bind
function boilWater(){};

var cookout={
  grill: function(duration, meat, name){
    console.log("Thanks " + name + "! Your " + meat + "will be ready in " + duration + "minutes.");
  }
}
 
var fancyDinner = {
  useOven: function() {}
}
 
var dessert = {
  bakeCake: function() {}
}

var cookBurger = cookout.grill.bind(cookout, 15, "burger");
 
var cookChicken = cookout.grill.bind(cookout, 20, "chicken");
 
var cookSteak = cookout.grill.bind(cookout, 10, "steak");

cookBurger("Jack");
cookChicken("Jack");
cookSteak("Jack");
//call

var cookout = {
  drink:"soda",
  grill: function(meal) {
    console.log("I am going to fire up the grill to cook " + meal + " with " +this.drink +" to drink!");
  }
}
 
var fancyDinner = {
  drink: "wine",
  useOven: function() {}
}
cookout.grill("steak");

//apply
var cookout = {
  mealOrders: ["chicken", "burger", "burger", "steak", "chicken"],
  grill: function() {
    var args = Array.prototype.slice.call (arguments);
 
    console.log("I am going to cook :" + args.join(","));
  }
 
}

cookout.grill.apply(cookout, this.mealOrders);


