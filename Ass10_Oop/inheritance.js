var Animal = function(name,age){
    this.animalname=name;
    this.animalage=age;
 };

Animal.prototype.getInfo = function(){
  return "Animal "+this.animalname +" " + this.animalage;
}

var Dog= function(name,age,type){
  Animal.call(this,name,age);
  this.dogtype=type;
};
Animal.prototype.getdogInfo = function(){
  return "Animal is "+this.animalname+" age is "+this.animalage+" year and type is "+this.dogtype;
}

Dog.prototype = Object.create(Animal.prototype);

var Human= function(name,age,address){
  Animal.call(this,name,age);
  this.address=address;
};
Animal.prototype.gethumanInfo = function(){
  return "Animal is "+this.animalname+" age is "+this.animalage+" year and address is "+this.address;
}

Human.prototype = Object.create(Animal.prototype);

var dog = new Dog("dog",4,"pug");
var human = new Human("human",44,"abcd");
dog.getdogInfo();
human.gethumanInfo();
