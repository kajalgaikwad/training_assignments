require_relative('./file_handler')
class Product
  attr_accessor  :product_id, :product_name, :company_name, :price, :quantity
  def initialize
      @product_id, @product_name, @company_name,@price, @quantity = 0
      @file_obj=FileHandler.new
  end

  def add_product
      get_product_details
      @file_obj.add_record(@product_array)
  end

  def get_product_details
     @product_array= Array.new
     puts "Enter the details of product which you want to add\n"
     puts "enter product_id"
     check_integer
     @product_id=@user_input
     puts "product_name"
     check_string
     @product_name = @user_input
     puts " company_name"
     check_string
     @company_name = @user_input
     puts "product_price"
     check_integer
     @price = @user_input
     puts "product_quantity"
     check_integer
     @quantity = @user_input
     @product_array.push("#{@product_id},#{@product_name},#{@company_name},#{@price},#{@quantity}")
  end

  def remove_product
     puts 'Enter the products item id you wish to delete.'
     check_integer
     @product_id = @user_input
     @file_obj.remove_filedata(@product_id)
  end


  def list_of_product
     data = ''
     file = File.open("inventory.txt", "r")
      file.each do |line|
      data += line
     end
     puts data
  end

  def search_product(search_name)
     file=File.open('inventory.txt', "r")
     file.each do |line|
      if line.match(/\b#{search_name}\b/i)
        @search_record = line
        puts line
      end
    end
  end

  def edit_product
     puts "enter the product name u want to edit"
     @search_name = gets.chomp
     search_product(@search_name)
     get_product_details
     data = @product_array.to_s
     @edited_record=@search_record.replace(data)
     @file_obj.add_record(@edited_record)
  end

  def check_integer
     while @user_input = Integer(gets.chomp) rescue ''
      if @user_input.is_a? Integer
        return @user_input
        break
      else
        print "Enter integer value only\n "
      end
     end
  end

  def check_string
     while @user_input =String(gets.chomp) rescue ''
      if @user_input.is_a? String
       return @user_input
       break
      else
       print "Enter string value only\n "
      end
    end
  end
end
