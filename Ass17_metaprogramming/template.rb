 require 'csv'
class Dynamic

 def initialize()
 @filename = "template.csv"
 @array = Array.new
  end

  def make_classname(filename)
   regex=/(.*)(^[a-z]*)/
   file=@filename.match(regex)
   class_name = file.to_s
   @class_name1=class_name.capitalize
  end

  def create_class(class_name1,header_attr)
   name = @class_name1
   attributes = @header_attr
    klass = Object.const_set name, Struct.new(*attributes)
    p klass.instance_methods(false)
    end

   def read_csv
     @row = CSV.parse(File.read(@filename), headers: false)
     attributes = @row[0]
     attributes.each do |ele|
     @header_attr=@array.push(ele.to_sym)
      end
    end

    def display_csv(row)
      @row.each do |ele|
        p ele
      end
    end
end

d= Dynamic.new
d.make_classname(@filename)
d.read_csv
d.display_csv(@row)
d.create_class(@class_name1,@header_attr)

