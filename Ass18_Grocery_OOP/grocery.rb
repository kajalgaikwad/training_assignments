class Item
  Initialize
    @item_price, @item_name, @item_offerprice
  end

  def create_list
    get item_name
    get item_count
  end

  def item_price
    @item_unitprice
    @item_offer_price
    @item_rule
  end
end

class Bill
 attr_accessor :item, :price, :item_count, :offer_price

 def Initialize
  @item, @item_price =0
 end

 def bill(item, item_count)
   check rule of item

   if item_has_offer
     return item_count * item_price
   else
     calculate simple item price
    end
  end
end

class GroceryStore
  def Initialize
    @rules, @hash
  end

  def total
    @list_of_item & @item_count
    @total = @bill(item,item_count)
  end
end
