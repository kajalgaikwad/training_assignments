$(document).ready(function(){
 var s,
 switchboard = {
   settings: {
     button: '#switch',
     board:'#switchboard',
     checkbox:'#checkbox',
   },

   init:function(){
     s = this.settings;
     this.bindUIActions();
     this.dragrows();
     console.log('init');
   },

   dragrows: function(){
      $("#rows").sortable({connectWith:"#dropbox",items: "li:not(:last-child)"});
      $("#dropbox").sortable({connectWith:"#rows"});
   },

   bindUIActions: function() {
     $(document).on('click', s.button, this.changecolor);
     $(document).on('click',s.checkbox,this.changecheckbox);
    },

   changecolor:function(){
   $("#switchboard").toggleClass("gray");
    if ($("#status").text() == "Switch Off"){
       $("#status").text("Switch On");
       $("#hello").text("Hello!!!");
     }
  else
     $("#status").text("Switch Off");
     $("#even1").toggleClass("gray");
     $("#even2").toggleClass("gray");
     $("#odd1").toggleClass("black");
      $("#odd2").toggleClass("black");
     setTimeout(function(){
       $('#hello').remove();
         }, 5000);
     },

   changecheckbox:function(){
    $("#switch").toggleClass("green");
   },
 }

 switchboard.init();
});