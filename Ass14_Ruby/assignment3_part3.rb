def calculator(a, b)
  yield(a, b)
end

puts calculator(20, 10) { |a, b| a + b }
puts calculator(20, 10) { |a, b| a - b }
puts calculator(20, 10) { |a, b| a * b }
puts calculator(20, 10) { |a, b| a / b }

 def compose num1,num2
    Proc.new do|ele|
      num2.call(num1.call(ele))
    end
  end
   proc_square=Proc.new do |ele|
    ele*ele
   end
   proc_double=Proc.new do |ele|
     ele+ele
   end

proc_new=compose proc_double,proc_square
proc_new.call(10)

def compose num1,num2
    Proc.lambda do|ele|
      num2.call(num1.call(ele))
    end
  end
   proc_square=Proc.lambda do |ele|
    ele*ele
   end
   proc_double=Proc.lambda do |ele|
     ele+ele
   end

proc_new=compose proc_double,proc_square
proc_new.call(10)